import React, { Component } from 'react';
import LoginComponent from '../../Components/WebPages/LoginComponent/LoginComponent'
import HomeUserLogged from '../../Components/WebPages/HomeUserLogged/HomeUserLogged'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';

class RootContainer extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {}
  }

  render() {
    //console.log('root',this.props, localStorage.getItem('isLogged'))
    let HomeComponent = this.props.user.userLogged || localStorage.getItem('isLogged') == 'true'
      ? HomeUserLogged : LoginComponent
    return (
      <HomeComponent />
    );
  }
}

RootContainer.propTypes = {
};

const mapStateToProps = (state, ownProps) => {
  return state.user
}

export default connect(mapStateToProps)(RootContainer);