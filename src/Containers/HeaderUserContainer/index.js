import React, { Component } from 'react';
import { connect } from 'react-redux'
import HeaderUserComponent from '../../Components/HeaderUserComponent'
import { actionCreator } from '../../actions'
import PropTypes from 'prop-types';

class HeaderUserContainer extends Component {
  render() {
    console.log('props',this.props)
    return (
      <HeaderUserComponent { ...this.props }/>
    );
  }
}

HeaderUserContainer.propTypes = {

};

const mapDispathToProps = dispath => ({
  closeSession: value => dispath(actionCreator(value))
})

const mapStateToProps = state => (
  {
    isLoggedIn: state.user.isLogged
  }
)

export default connect(mapStateToProps, mapDispathToProps)(HeaderUserContainer)