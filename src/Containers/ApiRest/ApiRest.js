class ApiRest {
  constructor({ url, headers, query, method, body }) {
    this.url = url
    this.headers = headers
    this.query = query
    this.method = method
    this.body = body
  }

  set(prop, value) {
    this[prop] = value
    return this
  }

  request() {
    const init = {
      method: this.method
    }
    let url = this.url
    
    if (this.headers) {
      init['headers'] = this.headers
    }

    if (this.body) {
      init['body'] = this.body
    }

    if  (this.query) {
      url += '?'
      for(const prop in this.query){
        url += `${prop}=${this.query[prop]}&`
      }
      url = url.slice(0, - 1)
    }

    let request = {url, init}
    return request
  }

}

export default ApiRest