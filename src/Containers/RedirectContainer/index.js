import React, { Component } from 'react';
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route, Redirect, withRouter } from 'react-router-dom'
import RootContainer from '../RootContainer'
import TicketView from '../../Components/WebPages/TicketView/TicketView'
import PropTypes from 'prop-types';

class RedirectContainer extends Component {
  render() {
    const { isLoggedIn, component: Component, path } = this.props
    return <Route
    path={ path }
    {...this.rest}
      render={props =>
        isLoggedIn || localStorage.getItem('isLogged') == 'true' ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  }
}

RedirectContainer.propTypes = {

}

const maptStateToProps = (state, ownProps) => {
  return {
    isLoggedIn: state.user.userLogged,
  }
}

export default withRouter(connect(maptStateToProps)(RedirectContainer))