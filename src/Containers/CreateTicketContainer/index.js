import React, { Component } from 'react';
import { connect } from 'react-redux'
import { GetProyects, GetUsers, GetStates, getTkPriority } from '../../actions'
import CreateTicket from '../../Components/WebPages/CreateTicket/CreateTicket'

class CreateTicketContainer extends Component {
  componentDidMount() {
    this.props.GetProyects()
    this.props.GetUsers()
    this.props.GetStates()
    this.props.getTkPriority()
    localStorage.setItem('sort_tk_id', 'down')
  }

  render() {
    return (
      <CreateTicket {...this.props}/>
    );
  }
}

const mapStateToProps = state => {
  return {
    usersData: state.usersData,
    proyects: state.proyects,
    tkStates: state.tkStates,
    tkPriority: state.tkPriority
  }
}

const mapDispatchToProps = dispatch => {
  return {
    GetProyects: value => dispatch(GetProyects(value)),
    GetUsers: value => dispatch(GetUsers(value)),
    GetStates: value => dispatch(GetStates(value)),
    getTkPriority: value => dispatch(getTkPriority(value))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTicketContainer)