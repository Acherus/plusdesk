import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ListTicket, sortListBy } from '../../actions'
import ListTicketHome from '../../Components/ListTicketHome'

import PropTypes from 'prop-types'

class ListTicketHomeContainer extends Component {
  componentDidMount() {
    this.props.getListTicket()
    localStorage.setItem('sort_tk_id', 'down')
  }

  sortBy = (e) => {

    if (e.currentTarget.id == 'tk-id') {
      this.props.data.ticketList.sort((a, b) => {
        return localStorage.getItem('sort_tk_id') === 'down' 
          ? parseInt(b.id) - parseInt(a.id) : parseInt(a.id) - parseInt(b.id)
      })

      if (localStorage.getItem('sort_tk_id') == 'down') {
        localStorage.setItem('sort_tk_id', 'up')  
      }
      else {
        localStorage.setItem('sort_tk_id', 'down')
      }
      
    }
    this.props.sortListBy({data: this.props.data.ticketList})
  }

  render() {
    return <ListTicketHome sortBy={ this.sortBy } {...this.props} />
  }
}

ListTicketHomeContainer.propTypes = {

};

const mapStateToProps = state => {
  return {data: state.data}
}

const mapDispathToProps = dispath => ({
  getListTicket: value => dispath(ListTicket()),
  sortListBy: value => dispath(sortListBy(value))
})

export default connect(mapStateToProps, mapDispathToProps)(ListTicketHomeContainer)