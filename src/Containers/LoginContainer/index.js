import React, { Component } from 'react'
import { connect } from 'react-redux'
import LoginComponent from '../../Components/WebPages/LoginComponent/LoginComponent'
import { LogginMiddleware } from '../../actions'

class LoginContainer extends Component {
  handleSubmit = (values, dispatch) => {
    dispatch(LogginMiddleware(values))
    const { isLoggedIn, history } = this.props
    if (isLoggedIn || localStorage.getItem('isLogged') == 'true') {
      history.push('/')
    }
  }

  render() {
    return (
      <LoginComponent { ...this.props } onSubmit={ this.handleSubmit }/>
    );
  }
}

const maptStateToProps = state => {
  return {
    isLoggedIn: state.user.userLogged,
  }
}
export default connect(maptStateToProps)(LoginContainer)