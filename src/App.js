import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import HomeUserLogged from './Components/WebPages/HomeUserLogged/HomeUserLogged'
import LoginContainer from './Containers/LoginContainer'
import RedirectContainer from './Containers/RedirectContainer'
import TicketView from './Components/WebPages/TicketView/TicketView'
import CreateTicketContainer from './Containers/CreateTicketContainer'
import './App.css'


class App extends Component {

  render() {
    const { isLoggedIn } = this.props
    return (
      <Router>
        <Switch>
          <Route exact path="/login"
          render={props =>
            isLoggedIn == false || localStorage.getItem('isLogged') != 'true' ? (
              <LoginContainer {...props} />
            ) : (
              <Redirect
                to={{
                  pathname: "/",
                  state: { from: props.location }
                }}
              />
            )
          } />
          <RedirectContainer {...this.props} exact path="/" component={ HomeUserLogged } />
          <RedirectContainer {...this.props} excat path="/ticket/:id" component={ TicketView }  />
          <RedirectContainer {...this.props} exact path="/create/ticket" component={ CreateTicketContainer } />
        </Switch>
      </Router>
    )
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.user.userLogged
  }
}

export default connect(mapStateToProps)(App)