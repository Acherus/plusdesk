import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { root } from './../reducers'

const composeEval = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const strore = createStore(root, composeEval(applyMiddleware(thunk)))
