import ApiRest from '../Containers/ApiRest/ApiRest'

export const USER_LOGGED = 'USER_LOGGED'

export const USER_LOGIN_ERROR = 'USER_LOGIN_ERROR'

export const USER_LOG_OUT = 'User log out'

export const GET_DATA_TICKET = 'getTickets'

export const GET_DATA_PROYECTS = 'getProyects'

export const GET_DATA_USERS = 'getUsers'

export const GET_DATA_STATES = 'getTkStates'

export const GET_DATA_PRIORITY = 'getTkPriority'


export const actionCreator = value => ({type: value.type, payload:value.payload})

export const LogginMiddleware = payload => {
  return dispatch => {
    console.log(payload)
    /*const request = new ApiRest({
      url: '',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      query: {
        'grant_type': 'client_credentials'
      },
      method: 'POST',
    })*/
    let request = new ApiRest({
      url: 'https://coffeesell.herokuapp.com/app/users',
      method: 'GET'
    })
    const { url, init } = request.request()

    fetch(new Request(url, init))
      .then( response => {
        console.log('el respon',response.text().then(text => {
          const data = text && JSON.parse(text)
          console.log('data',data)
        }))
        //dispath(actionCreator(response))
      })
      .catch( error => {
        console.log('error on call api rest: ', error)
        return error
      })
    const requestDuimmie = {
      userData: {
        user: 1,
        userName: 'test',
      },
      userValidation: true,
      keep: payload.keepSession
    }

    localStorage.setItem('isLogged', requestDuimmie.userValidation)
    localStorage.setItem('userData', requestDuimmie.userData)
    localStorage.setItem('keepSession', requestDuimmie.keep)

    dispatch(actionCreator({
      type: USER_LOGGED,
      payload: requestDuimmie
    }))
  }
}

export const ListTicket = payload => {
  return dispath => {
    const { url, init } = new ApiRest({
      url: 'get/tickets-assigend-tickets',
      method: 'GET'
    })

    //fetch(new Request(url, init)).then().error()

    //Dumie data
    const listData = [
      {
        id: '10',
        subject: 'Cotización PC algo mas xD :v que se yo algo, :V :U :3 so much swag, LOL :u',
        applicant: 'Natalia',
        date: '22 de Agosto 2019'
      },
      {
        id: '9',
        subject: 'Algo diferente :v',
        applicant: 'Juan Betancourt',
        date: '22 de Agosto 2019'
      },
      {
        id: '35',
        subject: 'Nuevo subject v:',
        applicant: 'Jhonnatan',
        date: '22 de Agosto 2019'
      }
    ]

    dispath(actionCreator({
      type: GET_DATA_TICKET,
      payload: {
          ticketList: listData
      }
    }))
  }
}

export const sortListBy = payload => {
  return dispath => {
    let { data } = payload
    dispath(actionCreator({
      type: GET_DATA_TICKET,
      payload: {
        ticketList: data
      }
    }))
    return data;
  }
}

export const GetProyects = payload => {
  return dispatch => {
    const { url, init } = new ApiRest({
      url: 'get/proyects',
      method: 'GET'
    })
    return fetch(new Request(url, init))
      .then( response => {
        response.text().then(text => {
          const data = text && JSON.parse(text)
          dispatch(actionCreator({
            type: GET_DATA_PROYECTS,
            payload: {
              proyects: data
            }
          }))
        })
        //console.log('el respon', '')
        //dispath(actionCreator(response))
      })
      .catch( error => {
        console.log('error on call api rest: ', error)
        return error
      })

    const dumie = [
      {
        id: 1,
        name: 'M&K System'
      },
      {
        id: 2,
        name: 'Otro proyecto'
      },
    ]
    dispatch(actionCreator({
      type: GET_DATA_PROYECTS,
      payload: {
        proyects: dumie
      }
    }))

  }
}

export const GetUsers = payload => {
  return dispath => {
    const { url, init } = new ApiRest({
      url: 'get/users',
      method: 'GET'
    })
    //fetch(new Request(url, init)).then().error()

    const dumie = [
      {
        id: 1,
        name: 'natalia'
      },
      {
        id: 2,
        name: 'jhonnatan'
      },
      {
        id: 3,
        name: 'juan'
      },
    ]
    dispath(actionCreator({
      type: GET_DATA_USERS,
      payload: {
        usersData: dumie
      }
    }))

  }
}

export const getTkPriority = payload => {
  return dispath => {
    const { url, init } = new ApiRest({
      url: 'get/users',
      method: 'GET'
    })
    //fetch(new Request(url, init)).then().error()

    const dumie = [
      {
        id: 1,
        name: 'Alta'
      },
      {
        id: 2,
        name: 'Intermedia'
      },
      {
        id: 3,
        name: 'Baja'
      },
    ]
    dispath(actionCreator({
      type: GET_DATA_PRIORITY,
      payload: {
        tkPriority: dumie
      }
    }))

  }
}

export const GetStates = payload => {
  return dispath => {
    const { url, init } = new ApiRest({
      url: 'get/users',
      method: 'GET'
    })
    //fetch(new Request(url, init)).then().error()

    const dumie = [
      {
        id: 1,
        name: 'Abierto'
      },
      {
        id: 2,
        name: 'En progreso'
      },
      {
        id: 3,
        name: 'Cerrado'
      },
    ]
    dispath(actionCreator({
      type: GET_DATA_STATES,
      payload: {
        tkStates: dumie
      }
    }))

  }
}

export const pushTk = payload => {
  return dispatch => {
    const { assign, description, priority, proyects, reporter, title } = payload
    const tkData = {"id_ticket":29,
    "id_proyecto": proyects,
    "id_estado":1,
    "tipo_ticket":1,
    "id_cliente":10,
    "asignado_a":assign, 
    "asignado_por":reporter, 
    "prioridad":priority, 
    "medio_ticket":1}

    /*const {url, init} = new ApiRest({
      url: 'https://test.api.tigo.com/oauth/client_credential/accesstoken',
      method: 'PUT',
      body: tkData

      fetch(new Request(url, init)).then().error
    })*/
  }
}