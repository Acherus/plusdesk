import React, { Component } from 'react';
import './EnterpriseComponent.css'
import PropTypes from 'prop-types';

class EnterpriseComponent extends Component {

  TitleComponent(title) {
    return title ? (
        <h2 className="page-tap-l">
          { title }
        </h2>
      ) : null
  }

  render() {
    const { type, title } = this.props
    return (
      <div className={ type }>
        { this.TitleComponent(title) }
        <h3 className="ent-name-h">
          mesadeayuda.m&ksystems.com
        </h3>
      </div>
    );
  }
}

EnterpriseComponent.propTypes = {
  type: PropTypes.string.isRequired,
  title: PropTypes.string
};

export default EnterpriseComponent;