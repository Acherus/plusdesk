import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import './index.css'

class ListTicketHome extends Component {

  render() {
    let { data, sortBy } = this.props
    return (
      <table className="t-h-list">
        <thead>
          <tr className="tr-alt">
            <td className="id-tl">
              <input id="all_t" type="checkbox"/>
            </td>
            <td id="tk-id" onClick={ sortBy }>ID</td>
            <td>Asunto</td>
            <td>Solicitante</td>
            <td>Fecha de solicitud</td>
          </tr>
        </thead>
        <tbody>
          {
            data.ticketList && data.ticketList.map( ({ id, subject, applicant, date }, index) => (
              <tr className={(index => (
                index % 2 != 0 ? 'tr-alt' : ''
              ))(index)}>
                <td className="id-tl">
                  <input id={ id }  type="checkbox" />
                </td>
                <td>
                  { id }
                </td>
                <td>
                  <Link to={"/ticket/" + id}> {subject }</Link>
                </td>
                <td>
                  { applicant }
                </td>
                <td>
                  { date }
                </td>
              </tr>
            ))
          }
        </tbody>
      </table>
    );
  }
}

ListTicketHome.propTypes = {
  data: PropTypes.array.isRequired,
};

export default ListTicketHome;