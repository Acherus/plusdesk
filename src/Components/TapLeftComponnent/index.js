import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './TapLeftComponnent.css'

class TapLeftComponnent extends Component {
  render() {
    return (
      <div className="tab-container">
        <div className="tab-wrapper">
          <div className="tab-left">
            <p>Panel</p>
          </div>
          <div className="tab-left">
            <p>
              <Link to="create/ticket">Crear Ticket</Link>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default TapLeftComponnent;