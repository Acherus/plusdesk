import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './index.css'

class TicketUpdateCard extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { name, commit, date } = this.props
    return (
      <div className="card-t-update">
        <div className="card-l-img">
          <i className="far fa-image"></i>
        </div>
        <div className="card-r-description">
          <p>{ name } comento:</p>
          <p>{ commit }</p>
          <p>{ date }</p>
        </div>
      </div>
    )
  }
}

TicketUpdateCard.propTypes = {
  name: PropTypes.string.isRequired,
  commit: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired
}

export default TicketUpdateCard