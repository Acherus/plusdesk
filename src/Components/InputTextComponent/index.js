import React, { Component } from 'react'
import { Field } from 'redux-form'
import PropTypes from 'prop-types'
import './InputTextComponent.css'

const MyField = ({input, meta}) => (
  <div>
    <input {...input} />
    { 
      meta.error && <span>meta.error</span> 
    }
  </div>
)

class InputTextComponent extends Component {

  isRequired(value) {
    if (this.props.required == true) {
      return !value && 'Este campo es requerido'
    }
  }

  render() {
    const { label, type, fName } = this.props
    return (
      <div className="input-wrapper">
        <label 
          className="input-label"
          htmlFor={fName}>
          { label }
        </label>
        <Field
          className={"field-"+type}
          id={ fName }
          name={ fName }
          component="input"
          type={ type } />
      </div>
    )
  }
}
 
InputTextComponent.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  fName: PropTypes.string.isRequired,
}

export default InputTextComponent