import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './index.css'

class IndicatorsComponent extends Component {
  render() {
    const { title, indicators } = this.props
    return (
      <div className="indicator-wrapper">
        <h5>
          { title }
        </h5>
        <div className="inds-wrapper">
        {
          indicators.map( ({ value, name }) => {
            return <div className="ind-card">
              <p>
                { value }
              </p>
              <p>
                { name }
              </p>
            </div>
          })
        }
        </div>
      </div>
    )
  }
}

IndicatorsComponent.propTypes = {
  title: PropTypes.string.isRequired,
  indicators: PropTypes.array.isRequired,
}

export default IndicatorsComponent