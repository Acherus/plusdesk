import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './HeaderComponent.css'
import '../../fontawesome/css/all.css'

class HeaderComponent extends Component {
  render() {
    return (
      <header className="loaded-header">
        <div className="logo-header">
        </div>
        <div className="h-menu-container">
        <ul className="header-opt">
          <li>
            <Link to="/" className="home">
              <i className="fas fa-home"></i>
            </Link>  
          </li>
          <li>
            <Link to="/ticket-list" className="archive">
              <i className="fas fa-clipboard-list"></i>
            </Link>
          </li>
          <li>
            <Link to="/stadistics" className="stadistics">
              <i className="fas fa-signal"></i>
            </Link>
          </li>
          <li>
            <Link to="/settings" className="settings">
              <i className="fas fa-cog"></i>
            </Link>
          </li>
        </ul>
        </div>
      </header>
    );
  }
}

HeaderComponent.propTypes = {

};

export default HeaderComponent;