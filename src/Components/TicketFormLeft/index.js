import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import './index.css'

class TicketFormLeft extends Component {
  render() {
    return (
      <form className="form-ticket-left">
        <label htmlFor="form-opt">
          Formulario
        </label>
        <Field component="select"
          name="form-opt" id="form-opt">
          <option id="mksystem">Correo M&K</option>
        </Field>
        <label htmlFor="reason-tk">
          Causa de contacto
        </label>
        <Field 
          id="reason-tk" 
          name="reason" 
          type="text" 
          component="input" />
        <label htmlFor="country-tk">
          País
        </label>
        <Field 
          id="country-tk" 
          name="country" 
          type="text" 
          component="input" />
        <label htmlFor="city-tk">
          Ciudad
        </label>
        <Field 
          id="city-tk" 
          name="city" 
          type="text" 
          component="input" />
        
      </form>
    )
  }
}

TicketFormLeft.propTypes = {

}

export default reduxForm({form: 'ticket-l-edit'})(TicketFormLeft)