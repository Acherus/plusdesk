import React, { Component } from 'react'
import './index.css'
import PropTypes from 'prop-types'

class RightWrapper extends Component {
  render() {
    const { children, className } = this.props
    return (
      <div className={ ( () => ('right-wrapper ' + (className === undefined ? '' : className) ) )() }>
        { children }
      </div>
    )
  }
}

RightWrapper.propTypes = {
  children: PropTypes.element.isRequired,
  className: PropTypes.string,
}

export default RightWrapper