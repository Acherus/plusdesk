import React, { Component } from 'react'
import { USER_LOG_OUT } from '../../actions'
import './HeaderUserComponent.css'
import PropTypes from 'prop-types'

class HeaderUserComponent extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <header className="user-header">
        <div className="options-h-wrapper">
          <ul className="f-lvl-menu">
            <li className="h-el search-header">
              <i className="fas fa-search"></i>
            </li>
            <li className="h-el aplications">
              <i className="far fa-question-circle"></i>
            </li>
            <li className="h-el user-photo">
              <i className="far fa-user-circle"></i>
              <ul>
                <li onClick={ this.logOut }>
                  <a>
                    Cerrar Sesión
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </header>
    )
  }

  logOut = () => {
    localStorage.removeItem('isLogged')
    localStorage.removeItem('userData')
    localStorage.removeItem('keepSession')

    this.props.closeSession({
      type: USER_LOG_OUT,
      payload: {}
    })

    const { isLoggedIn, history } = this.props
    if (isLoggedIn == false || localStorage.getItem('isLogged') == null) {
      history.push('/login')
    }
  }
}

HeaderUserComponent.propTypes = {
  closeSession: PropTypes.func.isRequired,
}

export default HeaderUserComponent