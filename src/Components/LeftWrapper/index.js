import React, { Component } from 'react';
import './LeftWrapper.css'
import PropTypes from 'prop-types';

class LeftWrapper extends Component {
  render() {
    const { children, className } = this.props
    const cName = 'left-wrapper-l ' + className 
    return (
      <div className={ cName }>
        { children }
      </div>
    );
  }
}

LeftWrapper.propTypes = {
  children: PropTypes.element.isRequired,
};

export default LeftWrapper;