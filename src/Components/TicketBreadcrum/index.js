import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  TICKET_OPEN,
  TICKET_CLOSE,
  TICKET_PROGRESS
} from '../../Constants'
import './index.css'

class TicketBreadcrum extends Component {
  ticketStatus = (status, cssClass) => (
    cssClass + ' ' + status
  )

  render() {
    const { system, name, status, id } = this.props.breadProps
    return (
      <div className="bc-t-wrapper">
        <div className="bcs-t">
          <div className="bc-t">
            { system }
          </div>
          <div className="bc-t">
            { name }
          </div>
          <div className={ this.ticketStatus(status, 'bc-t') }>
            Ticket #{ id }
          </div>
        </div>
      </div>
    )
  }
}

TicketBreadcrum.propTypes = {
  breadProps: PropTypes.array.isRequired,
}

export default TicketBreadcrum