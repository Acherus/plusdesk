import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field } from 'redux-form'
import './index.css'

class FormCreateLeft extends Component {
  render() {
    console.log('form', this.props)
    return (
      <div className="tk-form-wrapper">
        <label htmlFor="proyects">Proyecto</label>
        <Field id="proyects" name="proyects" component="select" required>
          <option></option>
          {
            this.props.proyects.proyects.map(({id, name}) => (
              <option value={id}>{ name }</option>
            ))
          }
        </Field>
        <label htmlFor="reporter">Reportado por</label>
        <Field id="reporter" name="reporter" component="select" required>
          <option></option>
          {
            this.props.usersData.usersData.map(({id, name}) => (
              <option value={id}>{ name }</option>
            ))
          }
        </Field>
        <label htmlFor="assign">Asignado a</label>
        <Field id="assign" name="assign" component="select" required>
          <option></option>
          {
            this.props.usersData.usersData.map(({id, name}) => (
              <option value={id}>{ name }</option>
            ))
          }
        </Field>
        <label htmlFor="tkState">Estado:</label>
        <Field id="tkState" name="tkState" component="select" disabled required>
          <option value="1" selected>Abierto</option>
        </Field>
        <label htmlFor="priority">Prioridad</label>
        <Field id="priority" name="priority" component="select" required>
          <option></option>
          {
            this.props.tkPriority.tkPriority.map(({id, name}) => (
              <option value={id}>{ name }</option>
            ))
          }
        </Field>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  usersData: state.usersData,
  proyects: state.proyects,
  tkStates: state.tkStates,
  tkPriority: state.tkPriority
})

export default connect(mapStateToProps)(FormCreateLeft)