import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './WrapperLoggedComponent.css'

class WrapperLoogedComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      props: props
    }
  }

  render() {
    const { body } = this.state.props
    return (
      <div className="body-wrapper">
        { body }
      </div>
    );
  }
}

WrapperLoogedComponent.propTypes = {
  body: PropTypes.element.isRequired
};

export default WrapperLoogedComponent;