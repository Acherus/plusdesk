import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { reduxForm, Field } from 'redux-form'
import { Helmet } from 'react-helmet'
import ImputTextComponent from '../../InputTextComponent'
import './LoginComponent.css'

class LoginComponent extends Component {
  render() {
    console.log(this.props)
    const { handleSubmit } = this.props
    return (
      <div>
        <Helmet>
          <title>Plusdesk | Login</title>
          <meta charSet="utf-8"/>
        </Helmet>
        <div className="out-wrapper-logo">
          <div className="logo-login">
          </div>
        </div>
        <div className="login-wrapper">
          <div className="form-l-wrapper">
            <form onSubmit={ handleSubmit }>
              <h1 className="principal-t">
                Mesa de ayuda M&K System
              </h1>
              <ImputTextComponent 
                id="user" 
                fName="userLogin" 
                label="Email" 
                type="text"
              />
              <ImputTextComponent 
                id="pass" 
                fName="passLogin" 
                label="Contraseña" 
                type="password"
              />
              <div className="keep-l-wrapper">
                <Field
                  name="keepSession"
                  type="checkbox"
                  id="keep-session"
                  component="input"/>
                <label htmlFor="keep-session" id="keep-s-l">
                  Mantener sesión abierta
                </label>
              </div>
              <button
                type="submit"
                className="btn-green-p">
                Iniciar Sesión
              </button>
              <div className="wrapper-opt-log">
                <Link 
                  id="forget-pass"
                  to="/forget-pass">
                  Olvide mi contraseña
                </Link>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStatetToProps = state => {
  return {
    isLoggedIn: state.user.userLogged,
  }
}

export default reduxForm({form: 'login-form'})(LoginComponent)