import React, { Component } from 'react'
import PageTemplateLogged from '../PageTemplateLogged'
import EnterpriseComponent from '../../EnterpriseComponent'
import LeftWrapper from '../../LeftWrapper'
import RightWrapper from '../../RightWrapper'
import TapLeftComponnent from '../../TapLeftComponnent'
import TicketUpdateCard from '../../TicketUpdateCard'
import IndicatorsComponent from '../../IndicatorsComponent'
import ListTicketHomeContainer from '../../../Containers/ListTicketHomeContainer'
import { 
  MID_TITLE_HEADER, 
  NO_FULL_LEFT, 
  NO_FULL_RIGHT 
} from '../../../Constants'
//import ApiRest from '../../../Containers/ApiRest/ApiRest'
import './HomeUserLogged.css'

const data = [
  {
    name: 'Jhonnatan',
    commit: 'Se agrega un nuevo comentario de prueba.',
    date: 'Martes 28 de Mayo'
  },
  {
    name: 'Natalia',
    commit: 'Se agrega un nuevo comentario de prueba.',
    date: 'Martes 28 de Mayo'
  },
  {
    name: 'Juan Betancourt',
    commit: 'Se agrega un nuevo comentario de prueba.',
    date: 'Martes 28 de Mayo'
  },
  {
    name: 'Juan Betancourt',
    commit: 'Se agrega un nuevo comentario de prueba.',
    date: 'Martes 28 de Mayo'
  },
  {
    name: 'Juan Betancourt',
    commit: 'Se agrega un nuevo comentario de prueba.',
    date: 'Martes 28 de Mayo'
  },
]

const indData = [
  {
    title: 'Tickets Abiertos',
    data: [
      {
        value: 2,
        name: 'usted'
      }
    ]
  },
  {
    title: 'Estadisticas de tickets',
    data: [
      {
        value: 2,
          name: 'Buenos'
      },
      {
        value: 0,
          name: 'Malos'
      },
      {
        value: 20,
          name: 'Resueltos'
      }
    ]
  },
  {
    title: 'Estadisticas de satisfacción',
    data: [
      {
        value: '89%',
          name: 'HelpDesk'
      }
    ]
  }
]

class HomeUserLogged extends Component {
  render() {
    return (
      <PageTemplateLogged {...this.props} body={
        <div>
          <TapLeftComponnent />
          <LeftWrapper className={ NO_FULL_LEFT }  children={
            <div>
              <h4 className="lateral-t-l">
                Actualizaciones a sus tickets
              </h4>
              {
                 data.map(
                  ({ name, commit, date }) => {
                    return <TicketUpdateCard
                      name={ name }
                      commit={ commit }
                      date={ date } />
                  }
                )
              }
            </div>
          } />
          <EnterpriseComponent type={ MID_TITLE_HEADER } />
          <RightWrapper children={
            <div>
              <div className="t-ind-wrapper">
                {
                  indData.map( ({ title, data }) => (
                    <IndicatorsComponent indicators={ data }
                      title={ title } />
                  ))
                }
              </div>
              <ListTicketHomeContainer { ...this.props } />
            </div>
          }
          className={ NO_FULL_RIGHT } />
        </div>
      } title="Plusdesk | Home" />
    )
  }
}

export default HomeUserLogged
