import React, { Component } from 'react';
import PageTemplateLogged from '../PageTemplateLogged'
import TicketBreadcrum from '../../TicketBreadcrum'
import LeftWrapper from '../../LeftWrapper'
import RightWrapper from '../../RightWrapper'
import TicketFormLeft from '../../TicketFormLeft'
import {
  NO_FULL_LEFT,
  NO_FULL_RIGHT,
  TICKET_OPEN,
  TICKET_CLOSE,
  TICKET_PROGRESS
} from '../../../Constants'
import PropTypes from 'prop-types';

const data = {
  system: 'M&K System',
  name: 'Alejandro Lozano',
  status: TICKET_OPEN,
  id: 100
}

class TicketView extends Component {
  render() {
    return (
      <PageTemplateLogged {...this.props} body={
        <div>
          <TicketBreadcrum breadProps={ data } />
          <LeftWrapper className={ NO_FULL_LEFT }  children={
            <TicketFormLeft></TicketFormLeft>
          } />
          <RightWrapper children={
            <div>
              something 2
            </div>
          }
          className={ NO_FULL_RIGHT } />
        </div>
      } title="Plusdesk | Ticket" />
    );
  }
}

TicketView.propTypes = {

};

export default TicketView;
