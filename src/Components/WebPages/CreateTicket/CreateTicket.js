import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import PageTemplateLogged from '../PageTemplateLogged'
import LeftWrapper from '../../LeftWrapper'
import EnterpriseComponent from '../../EnterpriseComponent'
import RightWrapper from '../../RightWrapper'
import FormCreateLeft from '../../FormCreateLeft'
import './CreateTicket.css'

class CreateTicket extends Component {
  pushTk(e, dispatch) {
    console.log(e)
  }

  render() {
    console.log('aqui propiedades',this.props)
    const { handleSubmit } = this.props
    return (
      <PageTemplateLogged {...this.props} body={
        <form onSubmit={ handleSubmit(this.pushTk) }>
          <LeftWrapper {...this.props}  children={
            <div>
              <h4 className="lateral-t-l">
                Crear un nuevo Ticket
              </h4>
              <FormCreateLeft {...this.props}></FormCreateLeft>
              
            </div>
          } />
          <EnterpriseComponent />
          <RightWrapper children={
            <div className="form-tk-wrapper">
              <h2>Crear Ticket</h2>
              <label htmlFor="tk-title">Título del Ticket</label>
              <Field name="tk-title" id="tk-title" component="input" required/>
              <label htmlFor="description">Descripción</label>
              <Field name="description" id="description" component="textarea" required/>
              <button className="s-tk" type="submit">Crear</button>
            </div>
          }/>
        </form>
      } title="Plusdesk | Create Ticket" />
    );
  }
}

export default reduxForm({form: 'create-ticket'})(CreateTicket)