import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import HeaderComponent from '../HeaderComponent';
import HeaderUserContainer from '../../Containers/HeaderUserContainer';
import WrapperLoogedComponent from '../WrapperLoggedComponent';
import { Helmet } from 'react-helmet';

class PageTemplateLogged extends Component {

  render() {
    const { body, title } = this.props
    return (
      <div>
        <Helmet>
          <title>{ title }</title>
        </Helmet>
        <HeaderComponent />
        <HeaderUserContainer { ...this.props } />
        <WrapperLoogedComponent body={ body } />
      </div>
    )
  }
}

PageTemplateLogged.propTypes = {
  body: PropTypes.element.isRequired,
};

export default connect(null, null)(PageTemplateLogged)