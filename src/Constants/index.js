export const FULL_TITLE_HEADER = 'full-t-h'

export const MID_TITLE_HEADER = 'no-full-t-h'

export const NO_FULL_LEFT = 'no-full-l'

export const NO_FULL_RIGHT = 'no-full-r'

export const TICKET_OPEN = 't-open'

export const TICKET_CLOSE = 't-close'

export const TICKET_PROGRESS = 't-progress'