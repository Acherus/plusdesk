import { USER_LOGGED, USER_LOG_OUT} from '../actions'

const user = {
  user: {
    userData: {},
    userLogged: false
  }
}

export const LoginReducer = (state = user, action) => {
  switch (action.type) {
    case USER_LOGGED:
      const { userData, userValidation } = action.payload
      return { ...state, user: {userData, userLogged: userValidation} }
    case USER_LOG_OUT:
      return { ...state, user }
    default:
      return state
  }
}