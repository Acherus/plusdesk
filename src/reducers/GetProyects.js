import { GET_DATA_PROYECTS } from '../actions'

export const GetProyects = (state = {proyects:[]}, {type, payload}) => {
  switch(type) {
    case GET_DATA_PROYECTS:
      const { proyects } = payload
      return {...state, proyects}
    default:
      return state
  }
}