import { GET_DATA_USERS } from '../actions'

export const UsersInfo = (state = {usersData:[]}, {type, payload}) => {
  switch(type) {
    case GET_DATA_USERS:
      const { usersData } = payload
      return {...state, usersData}
    default:
      return state
  }
}