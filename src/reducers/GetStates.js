import { GET_DATA_STATES } from '../actions'

export const GetStates = (state = {tkStates:[]}, {type, payload}) => {
  switch(type) {
    case GET_DATA_STATES:
      const { tkStates } = payload
      return {...state, tkStates}
    default:
      return state
  }
}