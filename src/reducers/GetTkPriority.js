import { GET_DATA_PRIORITY } from '../actions'

export const GetTkPriority = (state = {tkPriority:[]}, {type, payload}) => {
  switch(type) {
    case GET_DATA_PRIORITY:
      const { tkPriority } = payload
      return {...state, tkPriority}
    default:
      return state
  }
}