import { combineReducers } from 'redux'
import { reducer as reduxForm } from 'redux-form'
import { LoginReducer } from './LoginReducer'
import { DataRequests } from './DataRequests'
import { UsersInfo } from './UsersInfo'
import { GetProyects } from './GetProyects'
import { GetStates } from './GetStates'
import { GetTkPriority } from './GetTkPriority'


export const root = combineReducers({ 
  form: reduxForm,
  user: LoginReducer,
  data: DataRequests,
  usersData: UsersInfo,
  proyects: GetProyects,
  tkStates: GetStates,
  tkPriority: GetTkPriority
})