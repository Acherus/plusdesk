import { GET_DATA_TICKET } from '../actions'

const initialState = {
    ticketList:[],
}

export const DataRequests = (state = initialState, { type, payload }) => {
  switch(type) {
    case GET_DATA_TICKET:
      let { ticketList } = payload
      return { ...state, ticketList }   
    default:
      return state
  }
}